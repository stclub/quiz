(function ($, window, document) {
    'use strict';
    // execute when the DOM is ready
    $(function() {
        var j_data  = $('.ids_meta').val();
        
        if(j_data == '[]') {
          $('#quiz_list_only').css('display', 'none');
        }
        var parent_hidden_val = $('#parent_hidden_val').val();
        if(parent_hidden_val == 0) {
          $('#answer_select').css('display', 'none');
          $('#add_quiz_t_d').css('display', 'none');
          $('#hidden_area').css('display', 'block');
        }
         var select_val_data = [];
         $('#add_quiz_t_d').on('click', function () {
          $('#quiz_list_only').css('display', 'block');
          
          var select_id = $('#answer_id_title').val();
          select_val_data.includes(select_id);
          
          if(select_val_data.includes(select_id) == false) {
            $( "#dialog" ).css('display', 'none');
            select_val_data.push(select_id);
            var select_val = $('#'+select_id).text();
            var select_html = $('#'+select_id+'_div').html();
             $('#'+select_id).css('display', 'none'); 
             if(select_html != undefined) {

              $('#select_data').append('<div id="select_data_'+select_id+'"></div>');
               $('#select_data_'+select_id).prepend(select_html+'<hr>');
             }else {
              $('#select_data_'+select_id).prepend('<hr>');
             }
            $('#select_data_'+select_id).prepend('<strong>Question:\xa0'  + select_val + '</strong><input type="hidden" name="parent_post_js_val[]" value="'+
              select_id+'" />');
             
          }else {
            $( "#dialog" ).css('display', 'block');
          }
          
        });
        $('body').on('click', '.remove_div', function () {
          var delete_div_id = $(this).attr('id');
          $('#select_data_'+delete_div_id).remove();
          $('#'+delete_div_id).css('display', 'block'); 
          const index_data = select_val_data.indexOf(delete_div_id);
          if (index_data > -1) {
            select_val_data.splice(index_data, 1);
          }
        });
         $('.delete').on('click', function () {
          var confirm_delete = confirm("Are you sure that you want to delete this item?");
          if(confirm_delete == false) {
             return;
          }
         
          var delete_id = $(this).attr('id');
            var up_id = delete_id.replace('delete', '');
            var u_id = up_id.split('_');
            var o_id = u_id[0];
            var post_id = u_id[1];
           
            /*=======json code start=================*/
            var json_record  = $('.ids_meta').val();
            var f_value = jQuery.parseJSON(json_record);
            f_value = f_value.filter(function(jsonObject) {
                return jsonObject.id != o_id;
            });
            var json_new_record = JSON.stringify(f_value);
            /*========json code end==================*/
            // jQuery post method, a shorthand for $.ajax with POST
            $.post(ajaxurl,                        
              {
                action: 'wporg_ajax_change_quiz',             
                quiz: json_new_record,
                current_post_id: post_id,
                nonce: $('#ajax_nonce_quiz').val()
              }, function (data) {
                 $('<h2 style="color:green">Quiz deleted Succesfully</h2>').insertAfter('#'+delete_id);
                 
                  $('#'+o_id+'_list_only').css('display', 'none');
                   var title_info = $('#'+o_id+'_list_only p').html();
                    var t_info = title_info.split(':');

                  
                  $('#answer_id_title').append('<option id="'+o_id+'" value="'+o_id+'">'+t_info[1]+'</option>');
                 
                    var index_d_data = select_val_data.indexOf(o_id);
                    if (index_d_data > -1) {
                      o_id.splice(index_d_data, 1);
                    }
                    $('#answer_select').css('display', 'block');
                    $('#add_quiz_t_d').css('display', 'block');
                    $('#hidden_area').css('display', 'none');
              }
            );
          });
    });
     
}(jQuery, window, document));
