/*jslint browser: true, plusplus: true */
(function ($, window, document) {
    'use strict';
    // execute when the DOM is ready
    $(function() {
        var status = $("#post_status").val();
        if(status == 'publish') {
          $("#add_quiz").css('display', 'none');
          $("#add_more_option").css('display', 'none');
        }
        $('.update').on('click', function () {
          $('#quiz_list').css('display', 'none');
          $("#add_quiz").css('display', 'block');
          $("#add_more_option").css('display', 'block');
            $('html, body').animate({scrollTop:'200px'}, 'slow');
           
        });
        $('#add_more_option').on('click', function () {
            var checked_option= []
            $('.option').each(function(i){
                checked_option[i] = $(this).val();
            });
            
            var last_character = checked_option.slice(-1);
            var lower_case = last_character.toString().toLowerCase();
            function nextChar(c) {
                var i = (parseInt(c, 36) + 1 ) % 36;
                return (!i * 10 + i).toString(36);
            }
     
            var new_value = nextChar(last_character);
            var alpha_val = new_value.toUpperCase();
            $('<br><label for="'+new_value+'_option">'
                +alpha_val+'</label><br><input type="Text" class="text_option" name="text_option[]" id="'
                +new_value+'_option">').insertAfter("#"+lower_case+"_option");
            $("#add_quiz").append('<input type="checkbox" id="'
                +new_value+'_correct_option" class="option" name="correct_option[]" value="'
                +alpha_val+'"><label for="'+new_value+'_option" style="margin:0 0 0 3px">'
                +alpha_val+'</label><br>');
            
            // jQuery post method, a shorthand for $.ajax with POST
            
        });
    });
     
}(jQuery, window, document));
