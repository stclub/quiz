<?php
namespace QuizPlugin;
class Script {
	public static function init() {
		add_action('admin_enqueue_scripts', array(__CLASS__, 'wporg_meta_box_scripts'));
	}
	public static function wporg_meta_box_scripts()
	{
		// get current admin screen, or null
	    $screen = get_current_screen();
	    // verify admin screen object
	    if (is_object($screen)) {
	        // enqueue only for specific post types
	        if (in_array($screen->post_type, ['quiz-question'])) {
	            
				 wp_register_script( 'wporg_meta_box_script', QUIZ_BASE_PATH . 'assets/js/add-meta-box.js', ['jquery']);
        		wp_enqueue_script( 'wporg_meta_box_script' );
	        }
	        if (in_array($screen->post_type, ['quiz'])) {
	            
	            wp_register_script( 'wporg_meta_box_quiz', QUIZ_BASE_PATH . 'assets/js/quiz.js', ['jquery']);
        		wp_enqueue_script( 'wporg_meta_box_quiz' );
	            
	        }
	    }
	}
}
