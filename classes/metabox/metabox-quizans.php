<?php
namespace QuizPlugin;
class Metabox_Quizans {
	public static function init() {
        
		add_action('add_meta_boxes', array(__CLASS__, 'quizans'));
	}
   public static function quizans()
    {
        $screens = array('quiz-question');
            add_meta_box(
                'quiz_ans_id',          // Unique ID
                'Question and Answer', // Box title
                'add_quizans_meta',   // Content callback, must be of type callable
                $screens[0]                  // Post type
            );
    }
}