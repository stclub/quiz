<?php
namespace QuizPlugin;
class Metabox_Quiz {
	public static function init() {
		add_action('add_meta_boxes', array(__CLASS__, 'quiz'));
	}
    public static function quiz()
    {
        $screens = array('quiz');
            add_meta_box(
                'quiz_id',          // Unique ID
                'Linked questions', // Box title
                'add_quiz_meta',   // Content callback, must be of type callable
                $screens[0]                 // Post type
            );
        
    }
}