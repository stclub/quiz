<?php
namespace QuizPlugin;
class Ajax_Quiz {
	public static function init() {
		add_action('wp_ajax_wporg_ajax_change_quiz', array(__CLASS__, 'wporg_meta_box_ajax_handler_quiz'));
	}
	public static function wporg_meta_box_ajax_handler_quiz()
	{
		if(isset($_POST['quiz'] ) ) {
		    if( wp_verify_nonce( $_POST['nonce'], 'ajax-nonce-quiz' ) ){
			
				$p_id = sanitize_text_field($_POST['current_post_id']);

				$quiz_info = sanitize_text_field(htmlentities($_POST['quiz']));

				if(current_user_can( 'edit_published_posts', $p_id, 'linked_questions' ) ) { 

					if( !update_post_meta( $p_id, 'linked_questions', $quiz_info ) ) {

						add_post_meta( $p_id, 'linked_questions', $quiz_info);
						
					}
		    		echo json_encode($quiz_info);
		    	}
		    }
	    }
	    // ajax handlers must die
	    die;
	}
}