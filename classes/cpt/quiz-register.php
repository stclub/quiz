<?php
namespace QuizPlugin;
class Quiz_Register {
	public static function init() {
		add_action('init', array(__CLASS__, 'quiz_register'));
	}
	public static function quiz_register() {

		$labels = array(
			'name' => _x('Quiz', 'post type general name'),
			'singular_name' => _x('Quiz ', 'post type singular name'),
			'add_new' => _x('Add New', 'Quiz'),
			'add_new_item' => __('Add New Quiz'),
			'edit_item' => __('Edit Quiz'),
			'new_item' => __('New Quiz'),
			'view_item' => __('View Quiz'),
			'search_items' => __('Search Quiz'),
			'not_found' =>  __('Nothing found'),
			'not_found_in_trash' => __('Nothing found in Trash'),
			'parent_item_colon' => ''
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array( 'title', 'editor' )
		  ); 

		register_post_type( 'quiz' , $args );
	}
}