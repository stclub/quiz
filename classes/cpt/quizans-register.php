<?php
namespace QuizPlugin;
class Quizans_Register {
	public static function init() {
		add_action('init', array(__CLASS__, 'quizans_register'));
	}
	public static function quizans_register() {

		$labels = array(
			'name' => _x('Quiz Question', 'post type general name'),
			'singular_name' => _x('Quiz Question Item', 'post type singular name'),
			'add_new' => _x('Add New', 'Quiz Question '),
			'add_new_item' => __('Add New Quiz Question '),
			'edit_item' => __('Edit Quiz Question '),
			'new_item' => __('New Quiz Question '),
			'view_item' => __('View Quiz Question '),
			'search_items' => __('Search Quiz Question'),
			'not_found' =>  __('Nothing found'),
			'not_found_in_trash' => __('Nothing found in Trash'),
			'parent_item_colon' => ''
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title', 'editor')
		  ); 

		register_post_type( 'quiz-question' , $args );
	}
}