<?php 
/**
Plugin Name: Quiz
description: Question and Answer
Version: 1.0
Author: Nadeem
License: MIT
Text Domain: quiz-system
Domain Path: /languages/
*/
    // If this file is accessed directory, then abort.
    defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
    define('QUIZ_BASE_PATH', plugin_dir_url(__FILE__));
    require_once(plugin_dir_path(__FILE__) . 'classes/index.php');
    require_once(plugin_dir_path(__FILE__) . 'templates/index.php');
    use QuizPlugin\Metabox_Quiz; 
    use QuizPlugin\Metabox_Quizans;  
    use QuizPlugin\Script;  
    use QuizPlugin\Ajax_Quiz;     
    use QuizPlugin\Quizans_Register;   
    use QuizPlugin\Quiz_Register;   

    add_action( 'plugins_loaded', 'quiz_system' );
    function quiz_system() {
        load_plugin_textdomain( 'quiz-system', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    	Metabox_Quiz::init();
    	Metabox_Quizans::init();
    	Script::init();
    	Ajax_Quiz::init();
    	Quizans_Register::init();
    	Quiz_Register::init();
    }   