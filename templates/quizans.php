<?php
function add_quizans_meta($post)
    {
         
        $value = get_post_meta($post->ID, 'linked_questions');
        if(isset($value) && !empty($value)) {
             $val_quiz_answer = json_decode(html_entity_decode($value[0]));
        }
        ?>
        <input type="hidden" id="ids_meta" value="<?php echo $value[0]; ?>" />
        <div id="add_quiz">
            <label for="Add_option"><?php  esc_html_e('Add Options:', 'quiz-system'); ?> </label><br>
            <?php
                if(isset($val_quiz_answer[0]->options)){
                        /*options*/

                        $option_count = $val_quiz_answer[0]->options; 
                        $str_small = 'a';
                        $str = 'A';
                        foreach($option_count as $option_val) {
                    ?>
                    
                    <label for="<?php echo $str_small; ?>_option">
                        <?php 
                            printf(
                                __( '%s', 'quiz-system' ),
                                $str
                            ); 
                           
                        ?>        
                    </label><br>
                   
                    <input type="Text" class="<?php esc_attr_e( 'text_option', 'quiz-system' ); ?>" name="text_option[]" id="<?php echo $str_small; ?>_option" value="<?php echo $option_val; ?>"><br>
                    
                    <?php
                         ++$str;
                         ++$str_small;
                        }
                                
                        /*options*/
                }else {
                 ?>
                 <label for="a_option"><?php  esc_html_e('A', 'quiz-system'); ?></label><br>
                <input type="Text" class="<?php esc_attr_e('text_option', 'quiz-system'); ?>" name="text_option[]" id="a_option"><br>
                <label for="b_option"><?php  esc_html_e('B', 'quiz-system'); ?></label><br>
                <input type="Text" class="<?php esc_attr_e('text_option', 'quiz-system'); ?>" name="text_option[]" id="b_option"><br>
                 <?php   
                } 
                if(isset($val_quiz_answer[0]->correct_option)) {

                    /*correct_option*/
                    ?>
                    <label for="correct_option"><?php esc_html_e('ANSWER OPTIONS: ', 'quiz-system'); ?></label><br>
                    <?php 
                        $string_small = 'a';
                        $string_capital = 'A';
                        $count = 0;
                        foreach($option_count as $option_val) {
                            if(isset($val_quiz_answer[0]->correct_option[$count])) {
                                 
                                $correct_option_val = $val_quiz_answer[0]->correct_option[$count]; 
                                if($correct_option_val == $string_capital){
                                    $check = 'checked';
                                    $count++;
                                    ?>
                                    <input type="checkbox" id="<?php echo $string_small; ?>_correct_option" class="<?php esc_attr_e('option'); ?>" name="correct_option[]" value="<?php echo $string_capital; ?>"  <?php echo $check; ?>>
                                    <label for="<?php  echo $string_small; ?>_option" style="margin-bottom:3px"><?php echo $string_capital; ?></label><br>
                                    <?php
                                        ++$string_capital;
                                        ++$string_small;
                                }else {
                                
                                        $check = '';
                                      
                                    ?>
                                    <input type="checkbox" id="<?php echo $string_small; ?>_correct_option" class="<?php esc_attr_e('option'); ?>" name="correct_option[]" value="<?php echo $string_capital; ?>"  <?php echo $check; ?>>
                                     <label for="<?php  echo $string_small; ?>_option" style="margin-bottom:3px"><?php echo $string_capital; ?></label><br>
                                    <?php
                                        ++$string_capital;
                                        ++$string_small;    
                                } 
                            }else {
                                
                                        $check = '';
                                       
                                    ?>
                                    <input type="checkbox" id="<?php echo $string_small; ?>_correct_option" class="<?php esc_attr_e('option'); ?>" name="correct_option[]" value="<?php echo $string_capital; ?>"  <?php echo $check; ?>>
                                     <label for="<?php  echo $string_small; ?>_option" style="margin-bottom:3px"><?php echo $string_capital; ?></label><br>
                                    <?php
                                        ++$string_capital;
                                        ++$string_small;    
                                } 
                            
                        }
                                
                    /*correct_option*/
                }else {
                    ?>
                    <label for="correct_option"><?php  esc_html_e('Correct Option', 'quiz-system'); ?></label><br>
                    <input type="checkbox" id="a_correct_option" class="<?php esc_attr_e('option', 'quiz-system'); ?>" name="correct_option[]" value="A">
                    <label for="a_option" style="margin-top:3px"><?php  esc_html_e('A', 'quiz-system'); ?></label><br>
                    <input type="checkbox" id="b_correct_option" class="<?php esc_attr_e('option', 'quiz-system'); ?>" name="correct_option[]" value="B">
                    <label for="b_option" style="margin-top:3px"><?php  esc_html_e('B', 'quiz-system'); ?></label><br>
                    <?php
                }
            ?>
        </div>
        <button type="button" id="add_more_option"><?php  esc_html_e('Add More Option', 'quiz-system'); ?></button>
        <input type="hidden" id="post_status" value="<?php echo get_post_status($post->ID); ?>" />
        <div id="quiz_list">
        
        <?php
        
        if(get_post_status($post->ID) == 'publish'){   
            if(isset($value)){
                
                if(isset($value[0])) {
                    $val_quiz_answer = json_decode(html_entity_decode($value[0]));
                
                ?>
           
                    <div id="<?php echo $post->ID; ?>">
                        <strong><?php 
                                esc_html_e('QUESTION: ', 'quiz-system' ); 
                              echo get_the_title($post->ID); 
                            ?>
                        </strong>
                        <br>
                        <br>
                        <strong>
                            <?php esc_html_e('ANSWER OPTIONS ', 'quiz-system'); ?>
        
                        </strong>
                        <?php 
                            $option_count = $val_quiz_answer[0]->options;
                            $str = 'A';
                            foreach($option_count as $option_val) {
                                if(isset($val_quiz_answer[0]->correct_option)) {
                                    if (in_array($str, $val_quiz_answer[0]->correct_option)) {
                                    ?>
                                      <b><p><?php echo $str; ?> : <?php echo $option_val; ?></p></b> 
                                    <?php 
                                    }else {
                                    ?>
                                        <p><?php echo $str; ?> : <?php echo $option_val; ?></p>
                                    <?php    
                                    }
                                }else {
                                    ?>
                                        <p><?php echo $str; ?> : <?php echo $option_val; ?></p>
                                    <?php  
                                }
                                
                                
                            ++$str;
                            }
                           
                        ?>
                        
                        <button type="button" id="<?php echo $val_quiz_answer[0]->id; ?><?php echo $post->ID; ?>" class="update"><?php esc_html_e('Update', 'quiz-system'); ?></button>
                        
                    </div>
                    <hr>
                <?php
                }
            }
            
        }
            $nonce = wp_create_nonce( 'ajax-nonce-quizans' );
            ?>

            <input type="hidden" id="ajax_nonce_quizans" name="nonce" value="<?php echo $nonce ?>" />
            
        </div>
        <?php
        
    }
    function save_quaizans_post($post_id)
    {
      
        if (array_key_exists('content', $_POST) && array_key_exists('text_option', $_POST)) {
            if( wp_verify_nonce( $_POST['nonce'], 'ajax-nonce-quizans' ) ){
                $val_id = array();
                $val_write_quiz = array();
                $val_options = array();
                $val_correct_option = array();
                $data_i = array(); 
                $val_id['id'] = $post_id;
                $val_write_quiz['write_quiz'] = $_POST['content'];
                $val_options['options'] = $_POST['text_option'];
                if(isset($_POST['correct_option'])) {
                    $val_correct_option['correct_option'] = $_POST['correct_option'];
                }else {
                    $get_c_option = get_post_meta($post_id, 'linked_questions');
                    $j_val = json_decode(html_entity_decode($get_c_option[0]));
                    $val_correct_option['correct_option'] = $j_val[0]->correct_option;
                }
                
                $data_i[] = array_merge($val_id, $val_write_quiz, $val_options, $val_correct_option);
         
                $j_data = json_encode($data_i);
                update_post_meta(
                    $post_id,
                    'linked_questions',
                    htmlentities($j_data)
                );
            }
            
        }
    }
    add_action('save_post', 'save_quaizans_post');
/**
 * Hide all metaboxes in the global $wp_meta_boxes
 */

add_filter( 'hidden_meta_boxes', function( $hidden, $screen)
{
    global $wp_meta_boxes;
    $cpt = 'quiz-question'; 

    if( $cpt === $screen->id && isset( $wp_meta_boxes[$cpt] ) )
    {
        $tmp = array();
        foreach( (array) $wp_meta_boxes[$cpt] as $context_key => $context_item )
        {
            foreach( $context_item as $priority_key => $priority_item )
            {
                foreach( $priority_item as $metabox_key => $metabox_item )
                     if ($metabox_key != 'quiz_ans_id' && $metabox_key != 'submitdiv'){
                        $tmp[] = $metabox_key;
                    }
            }
        }
        $hidden = $tmp;  
    }
    return $hidden;
}, 10, 3 );