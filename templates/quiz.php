<?php
    function add_quiz_meta($post) {
        $parents = get_posts(
            array(
                'post_type'   => 'quiz-question',
                'orderby'     => 'title',
                'order'       => 'ASC',
                'numberposts' => -1
            )
        );  
        $empty_var = 0;
        $value = get_post_meta($post->ID);
        if ( !empty( $parents ) ) {
            if(isset($value)){
                $parent_array = array();
                $parent_post_ids = array();
                foreach($value as $key => $val) {
                    
                    if ($key == 'linked_questions')  { 
                        $val_quiz_answer = json_decode(html_entity_decode($val[0]));

                        foreach($val_quiz_answer as $val_quiz_ans) {
                            $parent_array[] = get_the_title($val_quiz_ans->parent_id);
                            
                        }
                    }
                }
            }
            $emp_v = '';
            foreach ( $parents as $parent ) {
                    
                    if(!in_array($parent->post_title, $parent_array)){
                        $emp_v = 1;
                    }
                    
                }
            ?>
            <input type="hidden" id="parent_hidden_val" value="<?php echo $emp_v; ?>" />
            <div id="<?php echo esc_attr_e('add_quiz_only', 'quiz-system'); ?>">
                <p id="hidden_area" style="display: none;margin-bottom:-30px"><br>No questions left to assign.</p>
                <div id="answer_select">
                    <p><?php  esc_html_e('Select Quiz Question Post', 'quiz-system'); ?></p>
                        
                    <select id="answer_id_title" name="parent_id">'
                    <?php
                    $emp_var = '';
                    foreach ( $parents as $parent ) {
                        
                        if(!in_array($parent->post_title, $parent_array)){
                            
                            printf( '<option id="%s" value="%s"%s>%s</option>', esc_attr( $parent->ID ), esc_attr( $parent->ID ), selected( $parent->ID, $post->post_parent, false ), esc_html( $parent->post_title ) );
                            $emp_var = 1;
                           
                        }
                        
                    }
                    
                ?>
                    </select>
                </div>
            
               <!-- parent post data -->
            <?php
                foreach ( $parents as $parent ) {
                     $parent_value = get_post_meta($parent->ID);

                    if(isset($parent_value) && $parent_value != false){
                         
                        foreach($parent_value as $key => $val_p) {
                            
                            if ($key == 'linked_questions')  { 
                                
                                $val_quiz_answer_p = json_decode(html_entity_decode($val_p[0]));
                                foreach($val_quiz_answer_p as $val_quiz_ans_p) {
                                ?>
                                    <div style="display: none">
                                        <div id="<?php echo $parent->ID.'_div'; ?>">
                                            
                                            <div id="<?php echo $val_quiz_ans_p->id; ?>_quiz"> 
                                                <br>
                                                <?php 
                                                    esc_html_e('Description:', 'quiz-system' ) 
                                                   
                                                ?>
                                                <?php echo $val_quiz_ans_p->write_quiz; ?>
                                               
                                            </div>
                                             <br>
                                            <?php $option_count = $val_quiz_ans_p->options; ?>
                                            <strong>
                                                <?php esc_html_e('ANSWER OPTIONS ', 'quiz-system'); ?>
                            
                                            </strong>
                                            <?php 
                                                $str = 'A';
                                                foreach($option_count as $option_val) {
                                                    if(isset($val_quiz_ans_p->correct_option)) {
                                                        if (in_array($str, $val_quiz_ans_p->correct_option)) {
                                                            ?>
                                                              <b><p><?php echo $str; ?> : <?php echo $option_val; ?></p></b> 
                                                            <?php 
                                                        }else {
                                                        ?>
                                                            <p><?php echo $str; ?> : <?php echo $option_val; ?></p>
                                                        <?php    
                                                        }
                                                    }else {
                                                       ?>
                                                            <p><?php echo $str; ?> : <?php echo $option_val; ?></p>
                                                        <?php 
                                                    }
                                                    
                                                ++$str;
                                                }
                                               
                                            ?>
                                            
                                            <p><?php// esc_html_e('Correct Option', 'quiz-system'); ?> 
                                            <?php 
                                           /* if(is_array($val_quiz_ans_p->correct_option)){
                                                foreach($val_quiz_ans_p->correct_option as $correct_option_val) {
                                                    
                                                    if(isset($val_quiz_ans_p->options)){
                                                        $option_count = $val_quiz_ans_p->options; 
                                                        $str_ans = 'A';
                                                        foreach($option_count as $option_val) {
                                                            if($str_ans == $correct_option_val) {
                                                                echo '<strong>';
                                                                echo $correct_option_val . '-' . $option_val;
                                                                echo "&nbsp;&nbsp;";
                                                                echo '</strong>';
                                                            
                                                            }
                                                            ++$str_ans;
                                                        }
                                                    }
                                                }
                                            }*/
                                            ?></p>
                                            <button type="button" class="remove_div" id="<?php echo $parent->ID; ?>">Delete</button>
                                        </div>
                                    </div>
                                    
                            <?php
                                }
                            
                            }
                        }
                    
                    }
                    
                }

                ?>
                <!-- parent post data -->
                <br>
                <input type="hidden" id="current_post_id" name="current_post_id" value="<?php echo $post->ID ?>">
        
            </div>
            <button type="button" id="add_quiz_t_d"><?php esc_html_e('Assign', 'quiz-system'); ?></button>
        
           
            <div id="dialog" style="display:none">
                <h4>You already assign it. Try another one!</h4>
            </div>
            <h2 id="add_new_label" style="display:none; color:green"><?php esc_html_e('Quiz Assign successfully', 'quiz-system'); ?></h2>
        <?php
        }else {
                echo 'Add Question Answer post then linked here';
        }
        ?>     
                
        <div id="quiz_list_only">

        <?php
            if(isset($value) && !empty($value) ) {
               
                $count_n = 0;
                foreach($value as $key => $val) {
                   
                    if ($key == 'linked_questions')  { 

                         $count_n++;
                        ?>
                        <input type="hidden" class="ids_meta" name="ids_meta" value='<?php echo $val[0] ?>' />
                        <?php 
                        
                         if(isset($val_quiz_ans->parent_id)) {
                          
                            ?>
                            <hr><p style="margin-bottom:-20px"><b>Question List</b></p>
                             <div id="select_data"><br></div> 
                            <?php
                            $val_quiz_answer = json_decode(html_entity_decode($val[0]));
                            $count_number = 1;
                            foreach($val_quiz_answer as $val_quiz_ans) {
                            
                            ?>
                             
                                <div id="<?php echo $val_quiz_ans->id.'_list_only'; ?>">
                                    
                                    <p>
                                        <?php 
                                            printf(
                                                __( '<strong>Question %s:  %s</strong>', 'quiz-system' ),
                                                $count_number,
                                               get_the_title($val_quiz_ans->parent_id)
                                            ); 
                                           $count_number++;
                                        ?>
                                    </p>

                                    <!-- parent post data -->
                                    <?php
                                     $parent_value = get_post_meta($val_quiz_ans->parent_id);
                                    if(isset($parent_value) && $parent_value != false){
                                         
                                        foreach($parent_value as $key => $val_p) {
                                            
                                            if ($key == 'linked_questions')  { 
                                                
                                                $val_quiz_answer_p = json_decode(html_entity_decode($val_p[0]));
                                                
                                                foreach($val_quiz_answer_p as $val_quiz_ans_p) {
                                                ?>

                                                    <div id="<?php echo $val_quiz_ans_p->id.'_selected_post'; ?>">
                                                       
                                                        <div id="<?php echo $val_quiz_ans_p->id; ?>_quiz"> 
                                                            <?php 
                                                                esc_html_e('Description:', 'quiz-system' ) 
                                                               
                                                            ?>
                                                            <?php echo $val_quiz_ans_p->write_quiz; ?>
                                                        </div>
                                                        <br>
                                                        <?php $option_count = $val_quiz_ans_p->options; ?>
                                                       <strong>
                                                            <?php esc_html_e('ANSWER OPTIONS ', 'quiz-system'); ?>
                                        
                                                        </strong>
                                                        <?php 
                                                            $str = 'A';
                                                            foreach($option_count as $option_val) {
                                                              if(isset($val_quiz_ans_p->correct_option)) {
                                                                if (in_array($str, $val_quiz_ans_p->correct_option)) {
                                                                    ?>
                                                                      <b><p><?php echo $str; ?> : <?php echo $option_val; ?></p></b> 
                                                                    <?php 
                                                                }else {
                                                                ?>
                                                                    <p><?php echo $str; ?> : <?php echo $option_val; ?></p>
                                                                <?php    
                                                                }
                                                              }else {
                                                                ?>
                                                                    <p><?php echo $str; ?> : <?php echo $option_val; ?></p>
                                                                <?php
                                                              }
                                                                
                                                            ++$str;
                                                            }
                                                           
                                                        ?>
                                                        
                                                    </div>
                                                    
                                            <?php
                                                }
                                            
                                            }
                                        }
                                    
                                    }
                                    ?>
                                    <!-- parent post data -->
                                    <button type="button" id="<?php echo $val_quiz_ans->id; ?>delete_<?php echo $post->ID; ?>" class="<?php echo esc_attr_e('delete', 'quiz-system') ?>"><?php esc_html_e('Delete', 'quiz-system'); ?></button>
                                 <hr>   
                                </div>
                               
                            <?php
                            }
                        }
                    }else {
                        if($count_n == 0) {
                    ?>
                         <div id="select_data"><br></div>
                   <?php
                       }

                    }
                   
                }  
            }else {
               ?>
                 <div id="select_data"><br></div>
           <?php 
            }
            $nonce = wp_create_nonce( 'ajax-nonce-quiz' );
            ?>
            
            <input type="hidden" id="ajax_nonce_quiz" name="nonce" value="<?php echo $nonce ?>" />
        </div>
        
    <?php
    
    }
    function save_parent_post_title($post_id)
    {
        
        if (array_key_exists('parent_post_js_val', $_POST)) {
            if( wp_verify_nonce( $_POST['nonce'], 'ajax-nonce-quiz' ) ){
               $value = get_post_meta($post_id);
                if(isset($value)){
                    foreach($value as $key => $val) {
                        
                        if ($key == 'linked_questions')  { 
                            $val_quiz_answer = json_decode(html_entity_decode($val[0]));

                        }
                    }
                }
               
                $val_array_id_p = array();
                $val_array_parent_p = array();
                 $data_p = array();
                 $data_i = array();
                if(isset($val_quiz_answer)){
                    foreach($val_quiz_answer as $val_i) {
                        $val_array_id_p['id'] = $val_i->id;
                        $val_array_parent_p['parent_id'] = $val_i->parent_id;
                        $data_i[] = array_merge($val_array_id_p, $val_array_parent_p);
                        $j_data_i = json_encode($data_i);
                    
                    }
                }

                $val_array_id = array();
                $val_array_parent = array();
              
                $data = array();
               
                foreach($_POST['parent_post_js_val'] as $val) {
                   $val_array_id['id'] = $val;
                   $val_array_parent['parent_id'] = $val;
                   $data[] = array_merge($val_array_id, $val_array_parent);
                   
                }
                $end_data = array_merge($data_i, $data);
               
                $j_data = json_encode($end_data);
               
                update_post_meta(
                    $post_id,
                    'linked_questions',
                    htmlentities($j_data)
                );
                
            }
        }
    }
    add_action('save_post', 'save_parent_post_title'); 

    /**
 * Hide all metaboxes in the global $wp_meta_boxes
 */

add_filter( 'hidden_meta_boxes', function( $hidden, $screen)
{
    global $wp_meta_boxes;
    $cpt = 'quiz'; 

    if( $cpt === $screen->id && isset( $wp_meta_boxes[$cpt] ) )
    {
        $tmp = array();
        foreach( (array) $wp_meta_boxes[$cpt] as $context_key => $context_item )
        {
            foreach( $context_item as $priority_key => $priority_item )
            {
                foreach( $priority_item as $metabox_key => $metabox_item )
                     if ($metabox_key != 'quiz_id' && $metabox_key != 'submitdiv'){
                        $tmp[] = $metabox_key;
                    }
            }
        }
        $hidden = $tmp;  
    }
    return $hidden;
}, 10, 3 );